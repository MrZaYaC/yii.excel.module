<?php
/* @var $this DefaultController
 * @var $excel mixed
 */

$this->breadcrumbs=array(
	$this->module->id,
);
?>
<legend>Имрорт excel файла</legend>
<form class="form form-horizontal" method="post" enctype="multipart/form-data">
    <div class="control-group">
        <div class="control-label">
            <label>Выберите файл .xls</label>
        </div>
        <div class="controls">
            <?= CHtml::fileField('excel');?>
        </div>
    </div>
    <div class="form-actions">
        <?= CHtml::submitButton('Отправить', array('class' => 'btn btn-primary', 'name' => 'send'));?>
    </div>
</form>

<?php if($excel): ?>
<legend>Содержание файла</legend>
    <table class="table table-bordered">
        <?php foreach($excel as $row): ?>
        <tr>
            <?php foreach($row as $column): ?>
            <td><?= $column ?></td>
            <?php endforeach; ?>
        </tr>
        <?php endforeach;?>
    </table>
<?php endif;?>
