<?php

class DefaultController extends Controller
{
	public function actionIndex()
	{
        $excel = false;
        if(isset($_POST['send'])){
            if(!empty($_FILES['excel']['tmp_name'])){
                $file_path = $_FILES['excel']['tmp_name'];
                $excel = $this->getModule()->yexcel->readActiveSheet($file_path);
            }
        }
		$this->render('index', array(
            'excel' => $excel,
        ));
	}
}