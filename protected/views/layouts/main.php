<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/html" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="ru" />
    <link rel="icon" href="/images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon">
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <?php Yii::app()->bootstrap->registerAllCss(); ?>
    <?php Yii::app()->bootstrap->registerCoreScripts(); ?>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl.'/css/main.css'; ?>">
    <script type="text/javascript" src="<?= Yii::app()->baseUrl . '/js/main.js'?>"></script>
</head>

<body>
<div id="mainmenu">
    <?php $this->widget('bootstrap.widgets.TbNavbar', array(
    //    'type' => 'inverse',
    'fixed'=> 'top',
    'brand'=> Yii::app()->name,
    'brandUrl'=> Yii::app()->homeUrl,
    'collapse'=>true, // requires bootstrap-responsive.css
    'items'=>array(
        array(
            'class'=>'bootstrap.widgets.TbMenu',
            'items'=>array(
                array(
                    'label' => 'Excel',
                    'url' => Yii::app()->createUrl('excel'),
                    'active' => $this->activePage == Controller::PAGE_EXCEL,
                    'visible' => Yii::app()->user->isGuest
                ),
            ),
        ),
    ),
)); ?>
</div><!-- mainmenu -->
<div style="padding: 40px 0">

</div>
<div class="container" id="page">

    <?php if(isset($this->breadcrumbs)):?>
        <?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
        'links'=>$this->breadcrumbs,
    )); ?><!-- breadcrumbs -->
    <?php endif?>
    <?php echo $content; ?>

</div>
<div class="container">
	<div id="footer">

	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
