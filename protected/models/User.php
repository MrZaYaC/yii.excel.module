<?php

class User extends BaseUser
{
    const TYPE_ADMIN = 2;
    public $confirm_password;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return User the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array_merge(parent::rules(), array(
            array('confirm_password', 'required'),
            array('tel', 'match', 'pattern' => '/^[0-9]*$/', 'message' => 'Телефон должен состоять только из цифр'),
			array('tel', 'length', 'max'=> 10, 'min' => 10, 'message' => 'Телефон введен не правильно'),
            array('confirm_password', 'compare', 'compareAttribute'=>'password', 'message' => 'Пароли не совпадают'),
            array('email', 'email', 'message' => 'Не верный e-mail адрес'),
            array('login', 'match', 'pattern' => '/^[a-zA-Z0-9]+([_.-][a-zA-Z0-9]+)?$/', 'message' => 'Допускаются только латинские буквы, цифры и знаки ".", "-", "_"'),
		));
	}

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array_merge(parent::relations(),  array(
             'services' => array(self::MANY_MANY, 'Service', '{{user_has_service}}(user_id, service_id)'),
        ));
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array_merge(parent::rules(), array(
			'login' => 'Логин',
			'email' => 'e-mail',
			'password' => 'Пароль',
			'confirm_password' => 'Повтор пароля',
			'name' => 'Имя',
			'surname' => 'Фамилия',
			'tel' => 'Телефон',
			'balance' => 'Баланс',
		));
	}

    public function beforeValidate()
    {
        if($this->isNewRecord){
            $this->create_date = date('Y-m-d H:i:s');
            $criteria = new CDbCriteria();
            $criteria->condition = 'lower(login) = :login';
            $criteria->params = array(':login' => strtolower($this->login));
            if(User::model()->count($criteria) > 0)
                $this->addError('login', 'Такой логин уже есть в базе');

            $criteria = new CDbCriteria();
            $criteria->condition = 'lower(email) = :login';
            $criteria->params = array(':login' => strtolower($this->email));
            if(User::model()->count($criteria) > 0)
                $this->addError('email', 'Такой e-mail уже есть в базе');

            if(!empty($this->password))
                $this->password = md5($this->password);
            if(!empty($this->confirm_password))
                $this->confirm_password = md5($this->confirm_password);
        }
        return parent::beforeValidate();
    }

    public function beforeSave()
    {
        if($this->isNewRecord){
            $this->verified_email_code = md5(microtime(). $this->login . $this->email);
        }
        return parent::beforeSave();
    }

    public function validatePassword($password)
    {
        return $this->hashPassword($password)===$this->password;
    }

    public function hashPassword($password)
    {
        return md5($password);
    }

    public function isAdmin()
    {
        if(Yii::app()->user-isGuest()){
            return false;
        }
        $model = self::model()->findByPk(Yii::app()->user->getId());
        if($model->role == self::TYPE_ADMIN){
            return true;
        } else {
            return false;
        }
    }

    public function getHostingCount($id)
    {
        $criteria = new CDbCriteria();
        $criteria->with = array('service');
        $criteria->compare('user_id', $id);
        $criteria->compare('service.type', Service::TYPE_HOSTING);
        return UserHasService::model()->count($criteria);
    }

    public function getHostingForGrid($id = null)
    {
        if($id === null){
            $id = Yii::app()->user->getId();
        }
        $criteria = new CDbCriteria();
        $criteria->with = array('service');
        $criteria->compare('user_id', $id);
        $criteria->compare('service.type', Service::TYPE_HOSTING);
        $criteria->order = 'end_date';
        return new CActiveDataProvider(UserHasService::model(), array(
            'criteria'=> $criteria,
            'pagination'=>array(
                'pageSize'=>20,
            ),
        ));
    }
}