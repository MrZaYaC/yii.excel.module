<?php

/**
 * This is the model class for table "{{user}}".
 *
 * The followings are the available columns in table '{{user}}':
 * @property integer $id
 * @property string $login
 * @property string $email
 * @property string $password
 * @property string $name
 * @property string $surname
 * @property string $tel
 * @property string $balance
 * @property integer $role
 * @property string $create_date
 * @property string $last_login
 * @property integer $is_verified_email
 * @property string $verified_email_code
 *
 * The followings are the available model relations:
 * @property Invoice[] $invoices
 * @property Transaction[] $transactions
 * @property UserHasService[] $userHasServices
 */
class BaseUser extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return BaseUser the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{user}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('login, email, password, name, surname, tel, create_date', 'required'),
			array('role, is_verified_email', 'numerical', 'integerOnly'=>true),
			array('login, email, name, surname', 'length', 'max'=>255),
			array('password, verified_email_code', 'length', 'max'=>32),
			array('tel', 'length', 'max'=>12),
			array('balance', 'length', 'max'=>10),
			array('last_login', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, login, email, password, name, surname, tel, balance, role, create_date, last_login, is_verified_email, verified_email_code', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'invoices' => array(self::HAS_MANY, 'Invoice', 'user_id'),
			'transactions' => array(self::HAS_MANY, 'Transaction', 'user_id'),
			'userHasServices' => array(self::HAS_MANY, 'UserHasService', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'login' => 'Login',
			'email' => 'Email',
			'password' => 'Password',
			'name' => 'Name',
			'surname' => 'Surname',
			'tel' => 'Tel',
			'balance' => 'Balance',
			'role' => 'Role',
			'create_date' => 'Create Date',
			'last_login' => 'Last Login',
			'is_verified_email' => 'Is Verified Email',
			'verified_email_code' => 'Verified Email Code',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('login',$this->login,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('surname',$this->surname,true);
		$criteria->compare('tel',$this->tel,true);
		$criteria->compare('balance',$this->balance,true);
		$criteria->compare('role',$this->role);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('last_login',$this->last_login,true);
		$criteria->compare('is_verified_email',$this->is_verified_email);
		$criteria->compare('verified_email_code',$this->verified_email_code,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}