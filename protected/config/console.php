<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.

require_once('db.php');

return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Console Application',
	// application components
	'components'=>array(
		// uncomment the following to use a MySQL database
        'db'=>array(
            'connectionString' => 'mysql:host='.$db['host'].';dbname='.$db['database'],
            'emulatePrepare' => true,
            'username' => $db['username'],
            'password' => $db['password'],
            'charset' => 'utf8',
            'tablePrefix'=>'empty_',
            'enableProfiling' => true,
            'enableParamLogging' => true,
        ),
	),
);