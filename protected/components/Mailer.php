<?php

class Mailer extends CApplicationComponent
{
    public $adminEmails = array();

    private function sendMessage($template, $to, $subject, $params)
    {
        $message = new YiiMailMessage;
        $message->view = $template;
        $message->subject = $subject;
        $message->setBody(array('params' => $params), 'text/html');
        if (is_array($to))
        {
            foreach ($to as $email)
                $message->addTo($email);
        } else {
            $message->addTo($to);
        }

        $message->from = Yii::app()->params['adminEmail'];
        if(Yii::app()->mail->send($message)){
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $user User
     */
    public function onRegistration($user)
    {
        /**
         * @var $user User
         */
        if($this->sendMessage('registration', $user->email, 'Регистрация на evm.dn.ua', array('user' => $user))){
            return true;
        } else {
            return false;
        }
    }
}
